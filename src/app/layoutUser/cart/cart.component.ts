import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';



//Mô tả kiểu dữ liệu cho sản phẩm
class Product {
  'id':string;
  'name':string;
  'description':string;
  'thumnail': string;
  'oldPrice':number;
  'newPrice':number;
  'quantity':number;
}

class HotItem {
  'id':string;
  'name':string;
  'description':string;
  'thumnail': string;
  'oldPrice':number;
  'newPrice':number;
  'quantity':number;

}

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.css'],

})

export class CartComponent implements OnInit {

   //Khai báo product có KDL là mảng các Product
   products:Product[] =[
    { 
      'id':'1',
      'name':'Áo khoác denim họa tiết Monogram Star',
      'description':' Màu nâu/xám Taupe',
      'thumnail':'https://vn.louisvuitton.com/images/is/image/lv/1/PP_VP_L/louis-vuitton-%C3%A1o-kho%C3%A1c-bomber-hai-m%E1%BA%B7t--HON40WZLW900_PM2_Front%20view.png?wid=656&hei=656',
      'oldPrice': 890000,
      'newPrice': 700000,
      'quantity':2
    },
    {
      'id':'2',
      'name':'Áo Hoodie Họa Tiết LV Multi',
      'description':'Combat đã tay, chụp ảnh sắc nét',
      'thumnail':'https://vn.louisvuitton.com/images/is/image/lv/1/PP_VP_L/louis-vuitton-%C3%A1o-hoodie-h%E1%BB%8Da-ti%E1%BA%BFt-lv-multi-tools--HOY75WIHN900_PM2_Front%20view.png?wid=656&hei=656',
      'oldPrice': 3450000,
      'newPrice': 3200000,
      'quantity': 1
    },
    {
      'id':'3',
      'name':' Ba lô Multipocket',
      'description':' Ba lô đỉnh số 1 VN',
      'thumnail':'https://vn.louisvuitton.com/images/is/image/lv/1/PP_VP_L/louis-vuitton-ba-l%C3%B4-multipocket--M21846_PM2_Front%20view.png?wid=656&hei=656',
      'oldPrice': 592000,
      'newPrice': 580000,
      'quantity':1
    },
  ]

  hotItems:HotItem[] =[
    { 
      'id':'1',
      'name':'THC Full Spectrum: Natural',
      'description':' Xuất xứ: Pháp',
      'thumnail':'https://vn.louisvuitton.com/images/is/image/lv/1/PP_VP_L/louis-vuitton-%C3%A1o-s%C6%A1-mi-h%E1%BB%8Da-ti%E1%BA%BFt-thistle-trang-ph%E1%BB%A5c--HOS88WXI5744_PM2_Front%20view.png?wid=1240&hei=1240',
      'oldPrice': 512000,
      'newPrice': 475000,
      'quantity':2
    },
    {
      'id':'2',
      'name':'Black Dog',
      'description':'Combat đã tay, chụp ảnh sắc nét',
      'thumnail':'https://vn.louisvuitton.com/images/is/image/lv/1/PP_VP_L/louis-vuitton-ao-thun---HON01WJL4529_PM2_Front%20view.png?wid=656&hei=656',
      'oldPrice': 2450500,
      'newPrice': 2245000,
      'quantity': 1
    },
    {
      'id':'3',
      'name':' Boost THC Tincture - Indica',
      'description':' Boost đỉnh số 1 VN',
      'thumnail':'https://vn.louisvuitton.com/images/is/image/lv/1/PP_VP_L/louis-vuitton-%C3%A1o-kho%C3%A1c--HOL71EMJY413_PM2_Front%20view.png?wid=656&hei=656',
      'oldPrice': 592000,
      'newPrice': 580000,
      'quantity':1
    },
    {
      'id':'4',
      'name':'Ace Of Spade',
      'description':' Spade đỉnh số 1 VN',
      'thumnail':'https://vn.louisvuitton.com/images/is/image/lv/1/PP_VP_L/louis-vuitton-%C3%A1o-kho%C3%A1c-lv-x-yk-psychedelic-flower--HOL31EMJY506_PM2_Front%20view.png?wid=656&hei=656',
      'oldPrice': 592000,
      'newPrice': 580000,
      'quantity':1
    },
  ]

  getTotalPrice(products: Product[]): number {
    let total = 0;
    for (let i = 0; i < products.length; i++) {
      total += products[i].newPrice * products[i].quantity;
    }

    const formattedTotal = total.toLocaleString('vi-VN', {
      style: 'currency',
      currency: 'VND'
    });

    return total;

  }

  getTotalQuantity(products: Product[]): number {
    let totalQuantity = 0;
    for (let i = 0; i < products.length; i++) {
      totalQuantity += products[i].quantity;
    }
    return totalQuantity;
  }

  constructor(){};

 ngOnInit(){}

 // productID : KDL string, KQ trả về 1 hàm cx có thể khai báo KDL, void: là ko có KDL trả về
 removeProduct(productId:string):void{
     const index=this.products.findIndex(product=>product.id=== productId);
     this.products.splice(index,1);
 }

 //Update số lượng
 updateQuantity(element:any){
 console.log(element)
 }

  title = 'FE_DATN2023';
}
