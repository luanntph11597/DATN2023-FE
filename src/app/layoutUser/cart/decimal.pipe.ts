import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
@Pipe({ name: 'decimal' })
export class DecimalPipe implements PipeTransform {
  transform(value: number): string {
    if (isNaN(value) || value === null) return '';
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  }
}             
