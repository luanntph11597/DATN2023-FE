export const navigation = [
  {
    text: 'Trang chủ',
    path: '/home',
    icon: 'home'
  },
  {
    text: 'Sản phẩm',
    icon: 'folder',
    path: '/products',
    items: [
      {
        text: 'Áo',
        path: '/profile',
        items: [
          {
            text: 'Áo thun',
          },
          {
            text: 'Áo nỉ & Hoodie',
          },
          {
            text: 'Áo sơ mi & Áo kiểu',
          },
          {
            text: 'Áo Cardigan',
          },
          {
            text: 'Áo len',
          },
          {
            text: 'Áo bra',
          },
        ]
      },
      {
        text: 'Quần',
        items: [
          {
            text: 'Quần Jeans',
          },
          {
            text: 'Quần dài',
          },
          {
            text: 'Quần ống rộng',
          },
          {
            text: 'Quần legging',
          },
          {
            text: 'Quần tây',
          },
          {
            text: 'Quần nỉ',
          },
          {
            text: 'Quần short'
          }
        ]
      },
      {
        text: 'Chân váy',
      },
      {
        text: 'Đồ mặc trong & Đồ lót',
      },
      {
        text: 'Đồ mặc nhà',
      },
      {
        text: 'Đồ bầu',
      }
    ]
  },
  {
    text: 'Đơn hàng',
    icon: 'folder',
    items: [
      {
        text: 'Chờ thanh toán',
      },
      {
        text: 'Chờ vận chuyển',
      },
      {
        text: 'Chờ giao hàng',
      },
      {
        text: 'Chưa đánh giá',
      },
      {
        text: 'Đã hủy',
      }
    ]
  }
];
